DROP TABLE IF EXISTS `alm_user`;
CREATE TABLE `alm_user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) NOT NULL COMMENT '品牌名称',
  `logo` varchar(50) NOT NULL DEFAULT '' COMMENT '品牌Logo',
  `status` int NOT NULL COMMENT '状态',
  `create_time` datetime NOT NULL COMMENT 'create_time',
  `create_user` varchar(50) NOT NULL DEFAULT '' COMMENT 'createUser',
  `update_time` datetime NOT NULL COMMENT 'update_time',
  `update_user` varchar(50) NOT NULL DEFAULT '' COMMENT 'updateUser',
  `deleted` bigint NOT NULL DEFAULT '0' COMMENT 'deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='用户';

DROP TABLE IF EXISTS `alm_brand`;
CREATE TABLE `alm_brand` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) NOT NULL COMMENT '品牌名称',
  `logo` varchar(50) NOT NULL DEFAULT '' COMMENT '品牌Logo',
  `status` int NOT NULL COMMENT '状态',
  `create_time` datetime NOT NULL COMMENT 'create_time',
  `create_user` varchar(50) NOT NULL DEFAULT '' COMMENT 'createUser',
  `update_time` datetime NOT NULL COMMENT 'update_time',
  `update_user` varchar(50) NOT NULL DEFAULT '' COMMENT 'updateUser',
  `deleted` bigint NOT NULL DEFAULT '0' COMMENT 'deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='品牌';

/**
  门店
 */
DROP TABLE IF EXISTS `alm_shop`;
CREATE TABLE `alm_shop` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) NOT NULL COMMENT '品牌名称',
  `logo` varchar(50) NOT NULL DEFAULT '' COMMENT '品牌Logo',
  `status` int NOT NULL COMMENT '状态',
  `create_time` datetime NOT NULL COMMENT 'create_time',
  `create_user` varchar(50) NOT NULL DEFAULT '' COMMENT 'createUser',
  `update_time` datetime NOT NULL COMMENT 'update_time',
  `update_user` varchar(50) NOT NULL DEFAULT '' COMMENT 'updateUser',
  `deleted` bigint NOT NULL DEFAULT '0' COMMENT 'deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='品牌';