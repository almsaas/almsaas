package com.almsaas.clock.web.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(basePackages = "com.almsaas.clock.mapper")
public class MybatisFlexConfiguration {
}
