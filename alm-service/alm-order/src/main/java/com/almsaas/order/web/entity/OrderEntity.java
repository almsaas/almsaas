package com.almsaas.order.web.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

@Table("tnt_order")
public class OrderEntity {

	@Id(keyType = KeyType.Generator)
	private Long id;
}
