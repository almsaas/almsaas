package com.almsaas.order.web.mapper;

import com.almsaas.order.web.entity.OrderEntity;
import com.mybatisflex.core.BaseMapper;

public interface OrderMapper extends BaseMapper<OrderEntity> {
}
