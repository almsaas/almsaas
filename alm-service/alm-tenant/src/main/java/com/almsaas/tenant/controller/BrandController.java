package com.almsaas.tenant.controller;

import com.almsaas.core.common.annotation.IgnoreLogin;
import com.almsaas.core.common.api.ListRequest;
import com.almsaas.core.common.api.PagedList;
import com.almsaas.core.common.api.Result;
import com.almsaas.core.common.router.TenantRouter;
import com.almsaas.model.Brand;
import com.almsaas.tenant.model.input.BrandSubmitInput;
import com.almsaas.tenant.model.vo.BrandVo;
import com.almsaas.tenant.service.BrandService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/brand")
@TenantRouter
public class BrandController {

	@Autowired
	private BrandService brandService;

	/**
	 * 提交
	 * @param input
	 * @return
	 */
	@PostMapping("/submit")
	@Operation(summary = "提交")
	public Result submit(@Validated @RequestBody BrandSubmitInput input) {
		brandService.submit(input);
		return Result.success();
	}

	@PostMapping("/save")
	@Operation(summary = "提交")
	@IgnoreLogin
	public Result save(@RequestBody Brand brand) {
		brand.save();
		return Result.success();
	}

	/**
	 * 前端分页查询
	 * @param request
	 * @return
	 */
	@PostMapping("/list")
	public Result<PagedList<BrandVo>> list(@RequestBody ListRequest request) {
		return Result.success(brandService.list(request));
	}
}
