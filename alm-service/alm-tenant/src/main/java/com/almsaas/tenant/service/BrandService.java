package com.almsaas.tenant.service;

import com.almsaas.core.common.annotation.Transactional;
import com.almsaas.core.common.api.ListRequest;
import com.almsaas.core.common.api.PagedList;
import com.almsaas.tenant.model.input.BrandSubmitInput;
import com.almsaas.tenant.model.vo.BrandVo;

public interface BrandService {

	/**
	 * 新增/修改品牌
	 * @param input
	 */
	@Transactional
	void submit(BrandSubmitInput input);

	/**
	 * 品牌列表查询
	 * @param request
	 * @return
	 */
	PagedList<BrandVo> list(ListRequest request);
}
