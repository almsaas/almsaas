package com.almsaas.tenant.controller;

import com.almsaas.core.common.api.Result;
import com.almsaas.tenant.model.input.ShopSubmitInput;
import com.almsaas.tenant.service.ShopService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/shop")
public class ShopController {

	@Autowired
	private ShopService shopService;

	@PostMapping("/submit")
	public Result submit(@RequestBody ShopSubmitInput input) {
		return Result.success();
	}
}
