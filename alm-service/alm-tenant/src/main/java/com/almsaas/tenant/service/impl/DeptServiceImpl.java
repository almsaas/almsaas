package com.almsaas.tenant.service.impl;

import com.almsaas.tenant.model.input.DeptSubmitInput;
import com.almsaas.tenant.service.DeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DeptServiceImpl implements DeptService {
	@Override
	public void submit(DeptSubmitInput input) {

	}
}
