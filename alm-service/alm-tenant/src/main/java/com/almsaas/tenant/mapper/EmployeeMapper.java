package com.almsaas.tenant.mapper;

import com.almsaas.tenant.entity.EmployeeEntity;
import com.mybatisflex.core.BaseMapper;

public interface EmployeeMapper extends BaseMapper<EmployeeEntity> {


}
