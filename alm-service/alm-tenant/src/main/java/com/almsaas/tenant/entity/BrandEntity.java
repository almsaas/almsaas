package com.almsaas.tenant.entity;

import com.almsaas.core.common.entity.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import lombok.Data;

@Data
@Table("tnt_brand")
public class BrandEntity extends BaseEntity {

	@Column("name")
	private String name;

	@Column("logo")
	private String logo;

	@Column("status")
	private Integer stauts;
}
