package com.almsaas.tenant.service.impl;

import com.almsaas.core.common.api.ListRequest;
import com.almsaas.core.common.api.PagedList;
import com.almsaas.tenant.entity.BrandEntity;
import com.almsaas.tenant.mapper.BrandMapper;
import com.almsaas.tenant.model.input.BrandSubmitInput;
import com.almsaas.tenant.model.vo.BrandVo;
import com.almsaas.tenant.service.BrandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BrandServiceImpl implements BrandService {

	@Autowired
	private BrandMapper brandMapper;

	@Override
	public void submit(BrandSubmitInput input) {
		var brand = new BrandEntity();
		brand.fillInsert();
		brand.setName(input.getBrandName());
		brand.setLogo(input.getLogo());
		brand.setStauts(1);
		brandMapper.insertSelective(brand);
	}

	@Override
	public PagedList<BrandVo> list(ListRequest request) {
		return null;
	}
}
