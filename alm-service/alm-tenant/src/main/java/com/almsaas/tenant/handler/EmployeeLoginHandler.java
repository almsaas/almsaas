package com.almsaas.tenant.handler;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.almsaas.core.common.auth.LoginUser;
import com.almsaas.core.common.exception.BizException;
import com.almsaas.core.tenant.TenantKeyHolder;
import com.almsaas.tenant.entity.EmployeeEntity;
import com.almsaas.tenant.mapper.EmployeeMapper;
import com.mybatisflex.core.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Slf4j
public class EmployeeLoginHandler {

	@Autowired
	private EmployeeMapper employeeMapper;

	@Autowired
	private StringRedisTemplate redisTemplate;

	private static final String LOGIN_KEY = "employee:login:token:{}-{}";
	private static final String LOGIN_ERROR_COUNT = "employee:login:errorcount:{}-{}";

	/**
	 * 登陆
	 * @param account
	 * @param password
	 * @return
	 */
	public LoginUser login(String account, String password) {
		// 登陆次数过多，被锁定
		String tenantKey = TenantKeyHolder.getTenantKey();
		String loginErrorCountKey = StrUtil.format(LOGIN_ERROR_COUNT, tenantKey, account);
		String value = redisTemplate.opsForValue().get(loginErrorCountKey);
		if (NumberUtil.parseInt(value, 0) > 5) {
			throw new BizException("当前账号已被锁定");
		}

		// 校验密码
		var queryQrapper = new QueryWrapper();
//		queryQrapper.eq(EmployeeEntity::getAccount, account);
//		queryQrapper.eq(EmployeeEntity::getPassword, password);
		EmployeeEntity e = employeeMapper.selectOneByQuery(queryQrapper);
		if (e == null) {
			redisTemplate.opsForValue().increment(loginErrorCountKey);
			throw new BizException("用户名密码错误");
		}
		String token = UUID.randomUUID().toString().replaceAll("-", "");

		redisTemplate.opsForValue().set(StrUtil.format(LOGIN_KEY, tenantKey, token), JSONUtil.toJsonStr(e));
		var loginInfo = new LoginUser();
		loginInfo.setLoginId(e.getId());
		return loginInfo;
	}
}
