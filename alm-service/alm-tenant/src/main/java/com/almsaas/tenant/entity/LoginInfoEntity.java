package com.almsaas.tenant.entity;

import com.mybatisflex.annotation.Table;

import java.util.Date;

@Table("als_login_info")
public class LoginInfoEntity {

	private Long userId;

	private Integer userType;

	private String token;

	private String loginIp;

	private Date createTime;
}
