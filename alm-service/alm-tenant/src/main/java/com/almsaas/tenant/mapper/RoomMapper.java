package com.almsaas.tenant.mapper;

import com.almsaas.tenant.entity.RoomEntity;
import com.mybatisflex.core.BaseMapper;

public interface RoomMapper extends BaseMapper<RoomEntity> {
}
