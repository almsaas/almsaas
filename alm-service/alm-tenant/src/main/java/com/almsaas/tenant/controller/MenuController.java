package com.almsaas.tenant.controller;

import com.almsaas.core.common.api.Result;
import com.almsaas.user.api.dto.MenuDto;
import com.almsaas.tenant.service.MenuService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/menu")
public class MenuController {

	@Autowired
	private MenuService menuService;


	@Operation(summary = "获取用户的菜单列表")
	@GetMapping("/list")
	public Result<List<MenuDto>> list() {
		return Result.success(new ArrayList<>());
	}
}
