package com.almsaas.tenant.controller;

import com.almsaas.core.common.api.Result;
import com.almsaas.tenant.model.input.DeptSubmitInput;
import com.almsaas.tenant.service.DeptService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/dept")
public class DeptController {

	@Autowired
	private DeptService deptService;

	/**
	 * 提交
	 * @param input
	 * @return
	 */
	@Operation(summary = "新增/修改")
	@PostMapping("/submit")
	public Result submit(@RequestBody DeptSubmitInput input) {
		deptService.submit(input);
		return Result.success();
	}
}
