package com.almsaas.tenant.service;

import com.almsaas.tenant.model.input.SaveEmployeeInput;

public interface EmployeeService {

	/**
	 * 添加员工
	 * @param input
	 */
	void submit(SaveEmployeeInput input);
}
