package com.almsaas.tenant.mapper;

import com.almsaas.tenant.entity.BrandEntity;
import com.mybatisflex.core.BaseMapper;

public interface BrandMapper extends BaseMapper<BrandEntity> {


}
