package com.almsaas.tenant.mapper;

import com.almsaas.tenant.entity.MenuEntity;
import com.mybatisflex.core.BaseMapper;

public interface MenuMapper extends BaseMapper<MenuEntity> {
}
