package com.almsaas.tenant.controller;

import com.almsaas.core.common.api.Result;
import com.almsaas.tenant.model.input.SaveEmployeeInput;
import com.almsaas.tenant.service.EmployeeService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/emp")
public class StaffController {

	@Autowired
	private EmployeeService employeeService;

	@PostMapping("/submit")
	@Operation(summary = "添加员工")
	public Result submit(@RequestBody SaveEmployeeInput input) {
		employeeService.submit(input);
		return Result.success();
	}
}
