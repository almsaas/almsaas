package com.almsaas.tenant.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(basePackages = "com.almsaas.tenant.mapper")
public class MybatisConfiguration {
}
