package com.almsaas.tenant.entity;

import com.almsaas.core.common.entity.BaseEntity;
import com.mybatisflex.annotation.Table;
import lombok.Data;

@Table("tnt_employee")
@Data
public class EmployeeEntity extends BaseEntity {

	private String name;

	private String phone;

	private String account;

	private String password;

	private Integer lockStatus;
}
