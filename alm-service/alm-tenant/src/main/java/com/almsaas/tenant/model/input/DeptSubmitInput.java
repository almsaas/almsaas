package com.almsaas.tenant.model.input;

import lombok.Data;

@Data
public class DeptSubmitInput {

	private Long id;

	private String name;
}
