package com.almsaas.tenant.service;

import com.almsaas.core.common.auth.LoginUser;
import com.almsaas.tenant.model.input.LoginInput;

public interface LoginService {

	/**
	 * 登陆
	 * @param loginInput
	 */
	LoginUser login(LoginInput loginInput);
}
