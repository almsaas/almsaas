package com.almsaas.tenant.service;

import com.almsaas.tenant.model.input.DeptSubmitInput;

public interface DeptService {

	void submit(DeptSubmitInput input);
}
