package com.almsaas.tenant.controller;

import com.almsaas.core.common.annotation.IgnoreLogin;
import com.almsaas.core.common.api.Result;
import com.almsaas.core.common.auth.LoginUser;
import com.almsaas.tenant.model.input.LoginInput;
import com.almsaas.tenant.service.LoginService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class LoginController {

	@Autowired
	private LoginService loginService;

	/**
	 * 登陆
	 * @param input
	 * @return
	 */
	@Operation(description = "登陆")
	@PostMapping("/login")
	@IgnoreLogin
	public Result<LoginUser> login(@Validated @RequestBody LoginInput input) {
		return Result.success(loginService.login(input));
	}

	@GetMapping("/getLoginUser")
	public Result getLoginUser() {
		return Result.success();
	}

	@PostMapping("/loginByPhone")
	public Result loginByPhone() {
		return Result.success();
	}

	@PostMapping("/logout")
	public Result logout() {
		return Result.success();
	}
}
