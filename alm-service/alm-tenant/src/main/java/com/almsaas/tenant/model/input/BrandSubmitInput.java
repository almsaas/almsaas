package com.almsaas.tenant.model.input;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class BrandSubmitInput {

	@NotEmpty
	@Schema(description = "品牌名称")
	private String brandName;

	@Schema(description = "品牌logo")
	private String logo;
}
