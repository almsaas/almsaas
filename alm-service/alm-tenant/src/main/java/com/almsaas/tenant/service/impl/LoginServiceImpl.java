package com.almsaas.tenant.service.impl;

import com.almsaas.core.common.auth.LoginUser;
import com.almsaas.core.common.enums.UserType;
import com.almsaas.tenant.handler.EmployeeLoginHandler;
import com.almsaas.tenant.handler.MechLoginHandler;
import com.almsaas.tenant.model.input.LoginInput;
import com.almsaas.tenant.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

	@Autowired
	private EmployeeLoginHandler eLoginHandler;

	@Autowired
	private MechLoginHandler mLoginHandler;

	@Override
	public LoginUser login(LoginInput loginInput) {
		if (loginInput.getUserType() == UserType.STAFF.getCode()) {
			return eLoginHandler.login(loginInput.getAccount(), loginInput.getPassword());
		}
		return null;
	}
}
