package com.almsaas.tenant.entity;

import com.almsaas.core.common.entity.BaseEntity;
import com.mybatisflex.annotation.Table;

@Table("tnt_menu")
public class MenuEntity extends BaseEntity {

	private Long parentId;

	private String name;

	private String icon;
}
