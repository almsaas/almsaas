package com.almsaas.tenant.model.input;

import lombok.Data;

@Data
public class SaveEmployeeInput {

	private String phone;

	private String name;

	private String account;

	private String password;
}
