package com.almsaas.tenant.model.input;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class LoginInput {

	@NotEmpty(message = "账号名不能为空")
	@Schema(description = "账号名")
	private String account;

	@NotEmpty(message = "密码不能为空")
	@Schema(description = "密码")
	private String password;

	@Schema(description = "用户类型")
	private int userType ;
}
