package com.almsaas.tenant.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.almsaas.tenant.entity.EmployeeEntity;
import com.almsaas.tenant.model.input.SaveEmployeeInput;
import com.almsaas.tenant.mapper.EmployeeMapper;
import com.almsaas.tenant.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeMapper employeeMapper;

	@Override
	public void submit(SaveEmployeeInput input) {
		EmployeeEntity employeeEntity = new EmployeeEntity();
		BeanUtil.copyProperties(input, employeeEntity);
		employeeMapper.insert(employeeEntity);
	}
}
