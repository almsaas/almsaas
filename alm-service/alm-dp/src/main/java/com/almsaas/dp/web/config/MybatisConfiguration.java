package com.almsaas.dp.web.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(value = "com.almsaas.dp.web.mapper.platform",
	sqlSessionFactoryRef = "platformSqlSessionFactory", sqlSessionTemplateRef = "platformSqlSessionTemplate")
@MapperScan(value = "com.almsaas.dp.web.mapper.tenant",
	sqlSessionFactoryRef = "tenantSqlSessionFactory", sqlSessionTemplateRef = "tenantSqlSessionTemplate")
public class MybatisConfiguration {
}
