package com.almsaas.user.web.mapper.tenant;

import com.almsaas.user.web.entity.UserEntity;
import com.mybatisflex.core.BaseMapper;

public interface UserMapper extends BaseMapper<UserEntity> {
}
