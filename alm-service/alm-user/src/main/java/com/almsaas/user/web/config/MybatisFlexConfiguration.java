package com.almsaas.user.web.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(value = "com.almsaas.user.web.mapper.platform",
	sqlSessionFactoryRef = "platformSqlSessionFactory", sqlSessionTemplateRef = "platformSqlSessionTemplate")
@MapperScan(value = "com.almsaas.user.web.mapper.tenant",
	sqlSessionFactoryRef = "tenantSqlSessionFactory", sqlSessionTemplateRef = "tenantSqlSessionTemplate")
public class MybatisFlexConfiguration {

}
