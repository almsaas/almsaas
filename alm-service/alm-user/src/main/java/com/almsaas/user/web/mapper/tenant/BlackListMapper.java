package com.almsaas.user.web.mapper.tenant;

import com.almsaas.user.web.entity.BlackListEntity;
import com.mybatisflex.core.BaseMapper;

public interface BlackListMapper extends BaseMapper<BlackListEntity> {
}
