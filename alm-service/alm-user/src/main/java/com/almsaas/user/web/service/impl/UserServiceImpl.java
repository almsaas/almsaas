package com.almsaas.user.web.service.impl;

import com.almsaas.user.web.mapper.tenant.UserMapper;
import com.almsaas.user.web.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;

	@Override
	public void login() {

	}
}
