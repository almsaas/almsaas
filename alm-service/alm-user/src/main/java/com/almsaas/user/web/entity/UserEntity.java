package com.almsaas.user.web.entity;

import com.almsaas.core.common.entity.BaseEntity;
import com.mybatisflex.annotation.Table;
import lombok.Data;

@Data
@Table("tnt_user")
public class UserEntity extends BaseEntity {
}
