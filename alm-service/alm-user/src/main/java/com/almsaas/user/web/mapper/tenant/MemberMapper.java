package com.almsaas.user.web.mapper.tenant;

import com.almsaas.user.web.entity.MemberEntity;
import com.mybatisflex.core.BaseMapper;

public interface MemberMapper extends BaseMapper<MemberEntity> {
}
