package com.almsaas.user.web.entity;

import com.almsaas.core.common.entity.BaseEntity;
import com.mybatisflex.annotation.Table;
import lombok.Data;

@Data
@Table("tnt_member")
public class MemberEntity extends BaseEntity {
}
