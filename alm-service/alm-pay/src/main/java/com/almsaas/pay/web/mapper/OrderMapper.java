package com.almsaas.pay.web.mapper;

import com.almsaas.pay.web.entity.PayEntity;
import com.mybatisflex.core.BaseMapper;

public interface OrderMapper extends BaseMapper<PayEntity> {
}
