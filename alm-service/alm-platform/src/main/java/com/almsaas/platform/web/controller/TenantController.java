package com.almsaas.platform.web.controller;

import cn.hutool.core.util.RandomUtil;
import com.almsaas.platform.api.dto.TenantDto;
import com.almsaas.platform.web.service.TenantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tenant")
@Slf4j
public class TenantController {

	@Autowired
	private TenantService tenantService;

	/**
	 * 创建租户
	 * @param tenantDto
	 * @return
	 */
	@PostMapping("/create")
	public String createTenant(@RequestBody TenantDto tenantDto) {
		String tenantCode = RandomUtil.randomString(8);
		tenantDto.setTenantKey(tenantCode);
		tenantService.createTenant(tenantDto);
		return "OK";
	}
}
