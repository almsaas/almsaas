package com.almsaas.platform.web.service;

/**
 * @author moshenglei
 */
public interface SmsService {

	/**
	 * 发送验证码
	 * @param mobile
	 */
	void sendSmsCode(String tenantKey, String mobile);


	/**
	 * 校验验证码
	 * @param mobile
	 * @param smsCode
	 * @return
	 */
	boolean validate(String tenantKey, String mobile, String smsCode);
}
