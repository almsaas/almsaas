package com.almsaas.platform.web.service;

import com.almsaas.platform.web.model.input.CreateBrandInput;

public interface BrandService {

	void createBrand(CreateBrandInput input);
}
