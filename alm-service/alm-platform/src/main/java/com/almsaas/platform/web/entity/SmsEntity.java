package com.almsaas.platform.web.entity;

import com.almsaas.core.common.entity.BaseEntity;
import com.mybatisflex.annotation.Table;
import lombok.Data;

import java.util.Date;

/**
 * @author moshenglei
 */
@Data
@Table("als_sms")
public class SmsEntity extends BaseEntity {

	private String tenantKey;

	private String phone;

	private String smsCode;

	private String message;

	private Integer smsType;

	private String userIp;

	private Date createTime;
}
