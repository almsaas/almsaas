package com.almsaas.platform.web.entity;

import com.almsaas.core.common.entity.BaseEntity;
import com.mybatisflex.annotation.Table;
import lombok.Data;

@Table("als_tenant")
@Data
public class TenantEntity extends BaseEntity {

	private String name;

	private String tenantKey;

	private String jdbcUrl;

	private String userName;

	private String password;
}
