package com.almsaas.platform.web.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.almsaas.platform.api.dto.TenantDto;
import com.almsaas.platform.web.entity.TenantEntity;
import com.almsaas.platform.web.mapper.tenant.TenantMapper;
import com.almsaas.platform.web.service.TenantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TenantServiceImpl implements TenantService {

	@Autowired
	private TenantMapper tenantMapper;

	@Override
	public void createTenant(TenantDto tenantDto) {
		TenantEntity tenantEntity = new TenantEntity();
		BeanUtil.copyProperties(tenantDto, tenantEntity);
		tenantMapper.insert(tenantEntity);
	}
}
