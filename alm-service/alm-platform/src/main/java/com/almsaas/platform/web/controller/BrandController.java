package com.almsaas.platform.web.controller;

import com.almsaas.core.common.api.Result;
import com.almsaas.platform.web.model.input.CreateBrandInput;
import com.almsaas.platform.web.service.BrandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/plt/brand")
public class BrandController {

	@Autowired
	private BrandService brandService;

	/**
	 * 创建品牌
	 * @return
	 */
	@PostMapping("/create")
	public Result<String> submit(@RequestBody CreateBrandInput input) {
		brandService.createBrand(input);
		return Result.success();
	}
}
