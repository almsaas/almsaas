package com.almsaas.platform.web.mapper.platform;

import com.almsaas.platform.web.entity.SmsEntity;
import com.mybatisflex.core.BaseMapper;

/**
 * @author moshenglei
 */
public interface SmsMapper extends BaseMapper<SmsEntity> {


}
