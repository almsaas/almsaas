package com.almsaas.platform.web.feignimpl;

import com.almsaas.platform.api.TenantApi;
import com.almsaas.platform.api.dto.TenantDto;
import com.almsaas.platform.web.entity.TenantEntity;
import com.almsaas.platform.web.mapper.tenant.TenantMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TenantApiImpl implements TenantApi {

	@Autowired
	private TenantMapper tenantMapper;

	@Override
	public List<TenantDto> list() {
		List<TenantEntity> tenantEntities = tenantMapper.selectAll();
		List<TenantDto> tenantDtos = new ArrayList<>();
		for (TenantEntity tenantEntity: tenantEntities) {
			TenantDto tenantDto = new TenantDto();
			BeanUtils.copyProperties(tenantEntity, tenantDto);
			tenantDtos.add(tenantDto);
		}
		return tenantDtos;
	}
}
