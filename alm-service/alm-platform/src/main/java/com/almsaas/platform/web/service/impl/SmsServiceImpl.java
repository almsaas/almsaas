package com.almsaas.platform.web.service.impl;

import cn.hutool.core.util.PhoneUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.almsaas.core.common.enums.SmsType;
import com.almsaas.core.common.exception.BizException;
import com.almsaas.core.common.support.WebUtil;
import com.almsaas.platform.web.entity.SmsEntity;
import com.almsaas.platform.web.handler.SmsLimitHandler;
import com.almsaas.platform.web.mapper.platform.SmsMapper;
import com.almsaas.platform.web.service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Date;

/**
 * @author moshenglei
 */
@Service
public class SmsServiceImpl implements SmsService {

	private static final String SMS_CODE = "sms:code:{}-{}";

	@Autowired
	private SmsMapper smsMapper;

	@Autowired
	private SmsLimitHandler smsLimitHandler;

	@Autowired
	private StringRedisTemplate redisTemplate;

	@Override
	public void sendSmsCode(String tenantKey, String mobile) {
		// 校验手机号是否合法
		if (StrUtil.isEmptyIfStr(tenantKey)) {
			throw new BizException("tenantKey不能为空");
		}
		if (!PhoneUtil.isMobile(mobile)) {
			throw new BizException("手机号不合法");
		}
		String key = StrUtil.format(SMS_CODE, tenantKey, mobile);
		if (redisTemplate.hasKey(key)) {
			throw new BizException("发送验证码频繁");
		}
		// 同一手机号一个租户下一天只能发送10次
		smsLimitHandler.checkLimit(mobile, tenantKey);

		String smsCode = RandomUtil.randomNumbers(6);
		// 存储到redis
		redisTemplate.opsForValue().set(key, smsCode, Duration.ofSeconds(60));
		// 存一份到数据库
		SmsEntity smsEntity = new SmsEntity();
		smsEntity.setTenantKey(tenantKey);
		smsEntity.setSmsCode(smsCode);
		smsEntity.setPhone(mobile);
		smsEntity.setSmsType(SmsType.SMS_CODE.getCode());
		smsEntity.setUserIp(WebUtil.getIP());
		smsEntity.setCreateTime(new Date());
		smsMapper.insert(smsEntity);
	}

	@Override
	public boolean validate(String tenantKey, String mobile, String smsCode) {
		if (StrUtil.isEmptyIfStr(tenantKey)) {
			throw new BizException("tenantKey不能为空");
		}
		if (!PhoneUtil.isMobile(mobile)) {
			throw new BizException("手机号不合法");
		}
		String key = StrUtil.format(SMS_CODE, tenantKey, mobile);
		// 从redis里面取smsCode
		String realSmsCode = redisTemplate.opsForValue().get(key);
		if (realSmsCode == null) {
			throw new BizException("验证码已过期");
		}
		return realSmsCode.equals(smsCode);
	}
}
