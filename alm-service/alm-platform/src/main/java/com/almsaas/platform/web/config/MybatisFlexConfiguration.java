package com.almsaas.platform.web.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(value = "com.almsaas.platform.web.mapper.platform",
	sqlSessionFactoryRef = "platformSqlSessionFactory", sqlSessionTemplateRef = "platformSqlSessionTemplate")
@MapperScan(value = "com.almsaas.platform.web.mapper.tenant",
	sqlSessionFactoryRef = "tenantSqlSessionFactory", sqlSessionTemplateRef = "tenantSqlSessionTemplate")
public class MybatisFlexConfiguration {
}
