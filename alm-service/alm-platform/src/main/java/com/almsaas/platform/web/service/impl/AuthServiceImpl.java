package com.almsaas.platform.web.service.impl;

import com.almsaas.platform.web.handler.AuthHandler;
import com.almsaas.platform.web.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AuthServiceImpl implements AuthService {

	@Autowired
	private AuthHandler authHandler;

}
