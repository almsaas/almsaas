package com.almsaas.platform.web.feignimpl;

import com.almsaas.platform.api.CacheApi;
import com.almsaas.platform.api.dto.CacheRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class CacheApiImpl implements CacheApi {

	@Override
	public boolean check(CacheRequest cacheRequest) {
		return false;
	}

	@Override
	public void refresh(CacheRequest cacheRequest) {

	}
}
