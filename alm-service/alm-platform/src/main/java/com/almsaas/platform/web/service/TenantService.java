package com.almsaas.platform.web.service;

import com.almsaas.platform.api.dto.TenantDto;

public interface TenantService {

	/**
	 * 创建租户
	 * @param tenantDto
	 */
	void createTenant(TenantDto tenantDto);
}
