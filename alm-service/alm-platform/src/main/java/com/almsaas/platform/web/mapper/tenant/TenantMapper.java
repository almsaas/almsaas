package com.almsaas.platform.web.mapper.tenant;

import com.almsaas.platform.web.entity.TenantEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

public interface TenantMapper extends BaseMapper<TenantEntity> {

	@Update("create database ${database}")
	int createDatabase(@Param("database") String database);

	@Update("use ${database}; ${runSql}")
	int runSql(@Param("database") String database, @Param("runSql") String runSql);
}
