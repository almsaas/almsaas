package com.almsaas.platform.web.mapper.platform;

import com.almsaas.platform.web.entity.UserEntity;
import com.mybatisflex.core.BaseMapper;

public interface UserMapper extends BaseMapper<UserEntity> {
}
