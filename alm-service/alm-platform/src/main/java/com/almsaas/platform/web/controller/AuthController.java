package com.almsaas.platform.web.controller;

import com.almsaas.core.common.api.Result;
import com.almsaas.platform.web.model.input.AuthInput;
import com.almsaas.platform.web.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

	@Autowired
	private AuthService authService;

	@PostMapping("/auth")
	public Result auth(@RequestParam("webType") String webType,
					   @RequestParam("bizType") String bizType,
					   @RequestBody AuthInput authInput) {
		return Result.success("OK");
	}

	@PostMapping("/register")
	public Result register() {
		return Result.success();
	}
}
