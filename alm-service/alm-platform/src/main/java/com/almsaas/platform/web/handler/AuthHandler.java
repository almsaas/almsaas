package com.almsaas.platform.web.handler;

import com.almsaas.core.common.handler.BaseHandler;
import com.almsaas.platform.web.model.input.AuthInput;
import com.almsaas.platform.web.model.output.AuthOutput;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AuthHandler extends BaseHandler<AuthInput, AuthOutput> {
	@Override
	public AuthOutput handle(AuthInput params) {
		return null;
	}
}
