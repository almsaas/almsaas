package com.almsaas.platform.web.controller;

import com.almsaas.core.common.api.Result;
import com.almsaas.platform.web.service.SmsService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author moshenglei
 */
@RestController
@Slf4j
public class SmsController {

	@Autowired
	private SmsService smsService;

	/**
	 * 发送验证码
	 * @param mobile
	 * @return
	 */
	@Operation(summary = "发送验证码")
	@PostMapping("/sms/send")
	public Result sendSms(@RequestParam String mobile,
						  @RequestParam String tenant) {
		smsService.sendSmsCode(tenant, mobile);
		return Result.success();
	}
}
