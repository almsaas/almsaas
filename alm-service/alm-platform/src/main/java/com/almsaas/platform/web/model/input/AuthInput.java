package com.almsaas.platform.web.model.input;

import com.almsaas.core.common.handler.BizInput;
import lombok.Data;

@Data
public class AuthInput extends BizInput {

	private String usernmae;

	private String password;
}
