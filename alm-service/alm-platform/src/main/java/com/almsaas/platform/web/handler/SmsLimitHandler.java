package com.almsaas.platform.web.handler;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.almsaas.core.common.exception.BizException;
import com.almsaas.core.common.support.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class SmsLimitHandler {

	private static final String PHOME_SMS_LIMIT_DAY = "sms:limit:{}-{}-{}";
	private static final String IP_SMS_LIMIT_DAY = "sms:limit:{}-{}-{}";

	@Autowired
	private StringRedisTemplate redisTemplate;

	public void checkLimit(String phone, String tenantId) {
		checkDayLimit(phone, tenantId);
		checkIpLimit(phone, tenantId);
	}

	private void checkDayLimit(String phone, String tenantKey) {
		Date now = new Date();
		String day = DateUtil.format(now, "yyyy-MM-dd");
		Date endDay = DateUtil.endOfDay(now);
		String key = StrUtil.format(PHOME_SMS_LIMIT_DAY, tenantKey, phone, day, WebUtil.getIP());
		if (!redisTemplate.hasKey(key)) {
			redisTemplate.opsForValue().setIfAbsent(key, "1", endDay.getTime() - now.getTime(), TimeUnit.MILLISECONDS);
			return;
		}

		redisTemplate.opsForValue().increment(key, 1L);
	}

	private void checkIpLimit(String phone, String tenantKey) {
		Date now = new Date();
		String day = DateUtil.format(now, "yyyy-MM-dd");
		Date endDay = DateUtil.endOfDay(now);
		String key = StrUtil.format(IP_SMS_LIMIT_DAY, tenantKey, WebUtil.getIP(), day);
		if (!redisTemplate.hasKey(key)) {
			redisTemplate.opsForValue().setIfAbsent(key, "1", endDay.getTime() - now.getTime(), TimeUnit.MILLISECONDS);
			return;
		}
		String value = redisTemplate.opsForValue().get(key);
		if (StrUtil.isNotBlank(value) && NumberUtil.parseInt(value) > 10) {
			throw new BizException("当前IP发送验证码过多");
		}

		redisTemplate.opsForValue().increment(key, 1L);
	}
}
