package com.almsaas.flow.web.config;

import com.almsaas.core.tenant.DataSourceWrapper;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class FlowConfiguration {

	@Bean
	@ConfigurationProperties(prefix = "spring.datasource.flow")
	@Primary
	public DataSource flowDataSource() {
		var platformDataSource = new DataSourceWrapper();
		return platformDataSource;
	}

}
