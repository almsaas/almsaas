package com.almsaas.cloud.boot.starter;

import com.alibaba.cloud.commons.lang.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ConfigurableBootstrapContext;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.Map;

@Slf4j
public class NacosCloudRunListener implements SpringApplicationRunListener {

    private static final String NACOS_HOST = "47.100.98.178:8848";
    public void environmentPrepared(ConfigurableBootstrapContext bootstrapContext,
                                    ConfigurableEnvironment environment) {
        Map<String, Object> envMap = environment.getSystemProperties();
        String nacosEnv = environment.getProperty("nacosEnv");
        if (nacosEnv == null) {
            nacosEnv = "public";
        }
        // config
        envMap.put("spring.cloud.nacos.config.server-addr", NACOS_HOST);
        envMap.put("spring.cloud.nacos.config.namespace", nacosEnv);
        envMap.put("spring.cloud.nacos.config.shared-configs[0].data-id", "almsaas.yaml");
        envMap.put("spring.cloud.nacos.config.file-extension", "yaml");
        // discovery
        envMap.put("spring.cloud.nacos.discovery.server-addr", NACOS_HOST);
        envMap.put("spring.cloud.nacos.discovery.namespace", nacosEnv);
		String discoveryIp = environment.getProperty("discoveryIp");
		log.info("discoveryIp = {}", discoveryIp);
		if (StringUtils.isNotEmpty(discoveryIp)) {
			envMap.put("spring.cloud.nacos.discovery.ip", discoveryIp);
		}
    }
}
