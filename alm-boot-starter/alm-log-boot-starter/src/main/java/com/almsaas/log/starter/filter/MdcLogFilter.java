package com.almsaas.log.starter.filter;

import jakarta.servlet.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.io.IOException;
import java.util.UUID;

/**
 * @author moshenglei
 */
public class MdcLogFilter implements Filter {

	private static Logger log = LoggerFactory.getLogger(MdcLogFilter.class);

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		MDC.put("TRACE_ID", UUID.randomUUID().toString().replace("-", ""));
		long startTimestamp = System.currentTimeMillis();
		try {
			filterChain.doFilter(servletRequest, servletResponse);
		} finally {
			long endTimestamp = System.currentTimeMillis();
			log.info("当前请求耗时: {}", endTimestamp - startTimestamp);
		}
	}
}
