package com.almsaas.log.starter;

import com.almsaas.log.starter.filter.MdcLogFilter;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import java.util.Arrays;

/**
 * @author moshenglei
 */
@AutoConfiguration
@Import(value = {WebLogAspect.class})
public class LogAutoConfiguration {

	@Bean
	public FilterRegistrationBean mdcFilterRegistration() {
		MdcLogFilter mdcLogFilter = new MdcLogFilter();
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(mdcLogFilter);
		//注册该过滤器需要过滤的 url
		filterRegistrationBean.setUrlPatterns(Arrays.asList("/*"));
		return filterRegistrationBean;
	}
}
