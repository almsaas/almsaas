package com.almsaas.log.starter;

import cn.hutool.json.JSONUtil;
import com.almsaas.core.common.support.WebUtil;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
@Slf4j
public class WebLogAspect {

	/** 以 controller 包下定义的所有请求为切入点 */
	@Pointcut("@within(org.springframework.web.bind.annotation.RestController)")
	public void webLog() {}


	@Before("webLog()")
	public void doBefore(JoinPoint joinPoint) {
		HttpServletRequest request = WebUtil.getRequest();

		// 打印请求相关参数
		log.info("========================================== Start ==========================================");
		// 打印请求 url
		log.info("URL            : {}", request.getRequestURL().toString());
		// 打印 Http method
		log.info("HTTP Method    : {}", request.getMethod());
		// 打印调用 controller 的全路径以及执行方法
		log.info("Class Method   : {}.{}", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
		// 打印请求的 IP
		log.info("IP             : {}", request.getRemoteAddr());
		// 打印请求入参
		log.info("Request Args   : {}", JSONUtil.toJsonStr(joinPoint.getArgs()));
		log.info("========================================== End ==========================================");
	}
}
