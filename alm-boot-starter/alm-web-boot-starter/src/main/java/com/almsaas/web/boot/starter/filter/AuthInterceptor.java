package com.almsaas.web.boot.starter.filter;

import cn.hutool.core.util.StrUtil;
import com.almsaas.core.common.AppConstant;
import com.almsaas.core.common.annotation.IgnoreLogin;
import com.almsaas.core.common.cache.CacheNames;
import com.almsaas.core.common.exception.AuthException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

public class AuthInterceptor implements HandlerInterceptor {

	@Autowired
	private StringRedisTemplate redisTemplate;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if (!(handler instanceof HandlerMethod)) {
			return true;
		}
		HandlerMethod method = (HandlerMethod) handler;
		if (method.getMethod().getAnnotation(IgnoreLogin.class) != null) {
			return true;
		}
		if (method.getMethod().getDeclaringClass().getAnnotation(IgnoreLogin.class) != null) {
			return true;
		}
		var feignTicket = request.getHeader("FeignTicket");
		if (AppConstant.FeignTicket.equals(feignTicket)) {
			return true;
		}
		var token = request.getHeader("TOKEN");
		var value = redisTemplate.opsForValue().get(StrUtil.format(CacheNames.LOGIN_TOKEN, token));
		if (StrUtil.isBlank(value)) {
			throw new AuthException("未登陆");
		}
		return true;
	}
}
