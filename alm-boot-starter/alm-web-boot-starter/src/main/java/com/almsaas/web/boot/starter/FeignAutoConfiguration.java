package com.almsaas.web.boot.starter;

import com.almsaas.web.boot.starter.interceptor.FeignInterceptor;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;

@AutoConfiguration
public class FeignAutoConfiguration {

	@Bean
	public FeignInterceptor feignInterceptor() {
		return new FeignInterceptor();
	}
}
