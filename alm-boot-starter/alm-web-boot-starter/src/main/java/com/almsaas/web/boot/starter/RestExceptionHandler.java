package com.almsaas.web.boot.starter;

import cn.hutool.http.HttpStatus;
import com.almsaas.core.common.api.Result;
import com.almsaas.core.common.exception.BizException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestExceptionHandler {

	@ExceptionHandler(value = BizException.class)
	public Result handleError(BizException e) {
		return Result.fail(e.getMessage());
	}

	@ExceptionHandler(value = Exception.class)
	public Result handleError(Exception e) {
		return Result.fail(HttpStatus.HTTP_INTERNAL_ERROR, e.getMessage());
	}
}
