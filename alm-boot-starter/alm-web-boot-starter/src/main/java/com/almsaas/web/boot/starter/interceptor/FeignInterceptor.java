package com.almsaas.web.boot.starter.interceptor;

import cn.hutool.core.util.StrUtil;
import com.almsaas.core.common.AppConstant;
import com.almsaas.core.tenant.TenantKeyHolder;
import feign.RequestInterceptor;
import feign.RequestTemplate;

public class FeignInterceptor implements RequestInterceptor {

	@Override
	public void apply(RequestTemplate requestTemplate) {
		var tenant = TenantKeyHolder.getTenantKey();
		if (StrUtil.isNotBlank(tenant)) {
			requestTemplate.header("TENANT", tenant);
		}
		requestTemplate.header("FeignTicket", AppConstant.FeignTicket);
	}
}
