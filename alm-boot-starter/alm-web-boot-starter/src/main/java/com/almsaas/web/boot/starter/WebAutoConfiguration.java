package com.almsaas.web.boot.starter;

import com.almsaas.web.boot.starter.filter.AuthInterceptor;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@AutoConfiguration
@Import(value = RestExceptionHandler.class)
public class WebAutoConfiguration implements WebMvcConfigurer {

	@Bean
	public WebMvcConfiguration webMvcConfiguration() {
		return new WebMvcConfiguration();
	}

	@Bean
	public AuthInterceptor authInterceptor() {
		return new AuthInterceptor();
	}
}
