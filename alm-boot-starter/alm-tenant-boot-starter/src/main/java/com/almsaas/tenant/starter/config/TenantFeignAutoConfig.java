package com.almsaas.tenant.starter.config;

import com.almsaas.tenant.starter.interceptor.FeignInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration(proxyBeanMethods = false)
@Import(value = {FeignInterceptor.class})
public class TenantFeignAutoConfig {
}
