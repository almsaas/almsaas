package com.almsaas.tenant.starter.config;

import com.almsaas.core.tenant.filter.TenantFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class TenantAutoConfiguration {

	@Bean
	public FilterRegistrationBean tenantRegisterFilter(){
		FilterRegistrationBean<TenantFilter> bean = new FilterRegistrationBean<>();
		bean.setOrder(1);
		bean.setFilter(new TenantFilter());
		bean.addUrlPatterns("/*");
		return bean;
	}
}
