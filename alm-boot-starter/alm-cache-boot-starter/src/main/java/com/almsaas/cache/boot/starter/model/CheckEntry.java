package com.almsaas.cache.boot.starter.model;

import lombok.Getter;

public class CheckEntry {

	@Getter
	private String cacheEntity;

	@Getter
	private String cacheKey;

	@Getter
	private int version;

	public CheckEntry(String entity, String key, int version) {
		this.cacheEntity = entity;
		this.cacheKey = key;
		this.version = version;
	}

	public String getKey() {
		return cacheEntity + "|" + cacheKey;
	}
}
