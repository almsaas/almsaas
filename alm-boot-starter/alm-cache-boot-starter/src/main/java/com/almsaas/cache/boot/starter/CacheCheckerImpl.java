package com.almsaas.cache.boot.starter;

import com.almsaas.cache.boot.starter.model.CheckEntry;
import com.almsaas.core.cache.checker.CacheChecker;

import java.util.HashMap;
import java.util.Map;

public class CacheCheckerImpl implements CacheChecker {

	private CacheCheckerWatch cacheCheckerWatch;

	private Map<String, CheckEntry> checkMap = new HashMap<>();

	public CacheCheckerImpl() {
		cacheCheckerWatch = new CacheCheckerWatch(this);
		cacheCheckerWatch.start();
	}

	@Override
	public void register(String cacheKey, String cacheEntity) {

	}

	@Override
	public boolean check(String cacheKey, String cacheEntity) {
		return false;
	}
}
