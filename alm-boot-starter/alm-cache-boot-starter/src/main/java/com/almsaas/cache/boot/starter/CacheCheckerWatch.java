package com.almsaas.cache.boot.starter;

import com.almsaas.core.cache.checker.CacheChecker;
import org.springframework.context.SmartLifecycle;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CacheCheckerWatch implements SmartLifecycle {

	private ScheduledExecutorService runExec;
	private CacheChecker checker;

	private boolean isRunning = false;

	public CacheCheckerWatch(CacheChecker checker) {
		this.checker = checker;
		this.runExec = Executors.newSingleThreadScheduledExecutor();
		this.runExec.schedule(() -> {
			runCheck();
		}, 5, TimeUnit.SECONDS);
	}

	private void runCheck() {

	}

	@Override
	public void start() {
		synchronized (this) {
			if (isRunning) {
				return;
			}
			isRunning = true;
		}
	}

	@Override
	public void stop() {

	}

	@Override
	public boolean isRunning() {
		return false;
	}
}
