package com.almsaas.core.captcha.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author moshenglei
 */
@Data
public class SlideCaptchaCheck {

	@NotBlank
	@Schema(description = "验证码id")
	private String captchaId;

	@Schema(description = "背景图片宽度")
	private Integer bgImageWidth;

	@Schema(description = "背景图片高度")
	private Integer bgImageHeight;

	@Schema(description = "滑块图片宽度")
	private Integer sliderImageWidth;

	@Schema(description = "滑块图片高度")
	private Integer sliderImageHeight;

	@Schema(description = "滑动开始时间")
	private Date startSlidingTime;

	@Schema(description = "滑动结束时间")
	private Date endSlidingTime;

	@Schema(description = "滑动轨迹")
	@NotEmpty
	private List<Track> trackList;

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Track {

		@NotNull
		@Schema(description = "x坐标")
		private Integer x;

		@NotNull
		@Schema(description = "y坐标")
		private Integer y;

		@NotNull
		@Schema(description = "时间")
		private Integer t;

		@NotNull
		@Schema(description = "事件")
		private String type;
	}
}
