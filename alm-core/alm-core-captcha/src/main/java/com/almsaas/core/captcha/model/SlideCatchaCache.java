package com.almsaas.core.captcha.model;

import lombok.Data;

/**
 * @author moshenglei
 */
@Data
public class SlideCatchaCache {

	private Integer bgImageWidth;

	private Integer bgImageHeight;

	private Integer sliderImageWidth;

	private Integer sliderImageHeight;

	private int randomX;

	private int randomY;
}
