package com.almsaas.core.captcha.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author moshenglei
 */
@Data
@Schema(description = "滑块校验返回值")
public class SlideCaptchaCheckInfo {

	@Schema(description = "验证码校验id")
	private String checkCaptchaId;
}
