package com.almsaas.core.captcha.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author moshenglei
 */
@Data
@Schema(description = "滑块验证码")
public class SlideCaptcha {

	@Schema(description = "验证码id")
	private String captchaId;

	@Schema(description = "背景图")
	private String bgImage;

	@Schema(description = "滑块")
	private String slideImgage;

	@Schema(description = "背景图片宽度")
	private Integer bgImageWidth;

	@Schema(description = "背景图片高度")
	private Integer bgImageHeight;

	@Schema(description = "滑块图片宽度")
	private Integer sliderImageWidth;

	@Schema(description = "滑块图片高度")
	private Integer sliderImageHeight;

	private int randomX;

	private int randomY;
}
