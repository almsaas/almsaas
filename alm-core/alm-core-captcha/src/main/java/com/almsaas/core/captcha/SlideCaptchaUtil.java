package com.almsaas.core.captcha;

import com.almsaas.core.captcha.model.SlideCaptcha;
import org.springframework.core.io.ClassPathResource;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author moshenglei
 */
public class SlideCaptchaUtil {

	public static SlideCaptcha doGenerateCaptchaImage(String imgUrl) throws Exception {
		ImageIO.setUseCache(false);
		BufferedImage background = ImageIO.read(new URL(imgUrl));
		return doGenerateCaptchaImage(background);
	}

	public static SlideCaptcha doGenerateCaptchaImage(BufferedImage background) throws Exception {
		var fixedTemplate = ImageIO.read(new ClassPathResource("slide/fixed.png").getInputStream());
		var activeTemplate = ImageIO.read(new ClassPathResource("slide/active.png").getInputStream());
		var maskTemplate = fixedTemplate;

		// 获取随机的 x 和 y 轴
		var randomX = randomInt(fixedTemplate.getWidth() + 5, background.getWidth() - fixedTemplate.getWidth() - 10);
		var randomY = randomInt(background.getHeight() - fixedTemplate.getHeight());

		var cutImage = cutImage(background, maskTemplate, randomX, randomY);
		overlayImage(background, fixedTemplate, randomX, randomY);

		overlayImage(cutImage, activeTemplate, 0, 0);

		var matrixTemplate = createTransparentImage(activeTemplate.getWidth(), background.getHeight());
		overlayImage(matrixTemplate, cutImage, 0, randomY);

		var slideCaptcha = new SlideCaptcha();
		slideCaptcha.setBgImage(transform(background, "jpg"));
		slideCaptcha.setSlideImgage(transform(matrixTemplate, "png"));
		slideCaptcha.setBgImageWidth(background.getWidth());
		slideCaptcha.setBgImageHeight(background.getHeight());
		slideCaptcha.setSliderImageWidth(matrixTemplate.getWidth());
		slideCaptcha.setSliderImageHeight(matrixTemplate.getHeight());
		slideCaptcha.setRandomX(randomX);
		slideCaptcha.setRandomY(randomY);
		return slideCaptcha;
	}

	public static String transform(BufferedImage bufferedImage, String transformType) throws IOException {

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, transformType, byteArrayOutputStream);

		//转换成字节码
		byte[] data = byteArrayOutputStream.toByteArray();
		String base64 = Base64.getEncoder().encodeToString(data);
		return "data:image/" + transformType + ";base64,".concat(base64);
	}

	public static BufferedImage createTransparentImage(int width, int height) {
		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		return bufferedImage;
	}

	public static void overlayImage(BufferedImage baseBufferedImage, BufferedImage coverBufferedImage,
									int x, int y) {
		// 创建Graphics2D对象，用在底图对象上绘图
		Graphics2D g2d = baseBufferedImage.createGraphics();
		// 绘制
		g2d.drawImage(coverBufferedImage, x, y, coverBufferedImage.getWidth(), coverBufferedImage.getHeight(), null);
		// 释放图形上下文使用的系统资源
		g2d.dispose();
	}

	public static BufferedImage cutImage(BufferedImage oriImage, BufferedImage templateImage, int xPos, int yPos) {
		// 模板图像矩阵
		int bw = templateImage.getWidth(null);
		int bh = templateImage.getHeight(null);
		BufferedImage targetImage = new BufferedImage(bw, bh, BufferedImage.TYPE_INT_ARGB);
		// 透明色
		for (int y = 0; y < bh; y++) {
			for (int x = 0; x < bw; x++) {
				int rgb = templateImage.getRGB(x, y);
				int alpha = (rgb >> 24) & 0xff;
				// 透明度大于100才处理，过滤一下边缘过于透明的像素点
				if (alpha > 100) {
					int bgRgb = oriImage.getRGB(xPos + x, yPos + y);
					targetImage.setRGB(x, y, bgRgb);
				}
			}

		}
		return targetImage;
	}

	protected static int randomInt(int origin, int bound) {
		return ThreadLocalRandom.current().nextInt(origin, bound);
	}

	protected static int randomInt(int bound) {
		return ThreadLocalRandom.current().nextInt(bound);
	}
}
