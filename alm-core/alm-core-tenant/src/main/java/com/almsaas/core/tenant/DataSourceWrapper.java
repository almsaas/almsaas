package com.almsaas.core.tenant;

import com.almsaas.core.tenant.transaction.TransactionObjectHolder;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.SQLException;

@Slf4j
public class DataSourceWrapper extends HikariDataSource {

	@Override
	public Connection getConnection() throws SQLException {
		log.info("getConnection from datasource");
		var conn = super.getConnection();
		conn = ConnectionWrapper.proxy(conn);
		var transObject = TransactionObjectHolder.get();
		if (transObject != null && transObject.isBeginTransaction()) {
			conn.setAutoCommit(false);
			transObject.addConn(this, conn);
		}
		return conn;
	}
}
