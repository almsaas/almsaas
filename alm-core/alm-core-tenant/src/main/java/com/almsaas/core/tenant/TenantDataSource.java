package com.almsaas.core.tenant;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@Slf4j
public class TenantDataSource extends DataSourceWrapper {

	@Override
	public Connection getConnection() throws SQLException {
		var tenantKey = TenantKeyHolder.getTenantKey();
		log.info("获取数据库连接tenantKey:{}", tenantKey);
		var conn = super.getConnection();
		if (StrUtil.isBlank(tenantKey)) {
			throw new RuntimeException("没有找到合适的数据源");
		}
		changeDataBase(conn, tenantKey);
		return conn;
	}

	private void changeDataBase(Connection conn, String database) throws SQLException{
		String sql = StrUtil.format("use {};", database);
		log.info("run sql: {}", sql);
		Statement stat = conn.createStatement();
		stat.execute(sql);
		stat.close();
	}
}
