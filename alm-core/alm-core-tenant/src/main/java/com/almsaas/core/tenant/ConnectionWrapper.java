package com.almsaas.core.tenant;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;

@Slf4j
public class ConnectionWrapper implements InvocationHandler {

	private Connection target;

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if (method.getName().equals("close")) {
			log.info("关闭连接{} ", target.getSchema());
		}
		return method.invoke(this.target, args);
	}

	public Connection bind(Connection conn) {
		this.target = conn;
		return (Connection) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[] {Connection.class}, this);
	}

	public static Connection proxy(Connection conn) {
		ConnectionWrapper connectionHandler = new ConnectionWrapper();
		return  connectionHandler.bind(conn);
	}
}
