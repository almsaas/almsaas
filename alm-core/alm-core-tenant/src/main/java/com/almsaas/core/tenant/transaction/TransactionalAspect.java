package com.almsaas.core.tenant.transaction;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import java.sql.Connection;
import java.sql.SQLException;

@Aspect
@Slf4j
public class TransactionalAspect {

	@Pointcut("@annotation(com.almsaas.core.common.annotation.Transactional)")
	public void annotationPoinCut(){}

	@Around("annotationPoinCut()")
	public void around(ProceedingJoinPoint joinPoint) throws Throwable {
		var transObject = TransactionObjectHolder.get();
		if (transObject == null) {
			transObject = new TransactionObject();
			transObject.setBeginTransaction(true);
		}
		try {
			joinPoint.proceed();
		} catch (Throwable e) {
			log.info("发生异常， rollback");
			rollback();
			throw e;
		}
		commit();
	}

	private void rollback() {
		var transObject = TransactionObjectHolder.get();
		if (transObject != null) {
			var connMap = transObject.getConnnMap();
			for (Connection conn : connMap.values()) {
				try {
					conn.rollback();
				} catch (SQLException e) {
					log.info("回滚失败");
					throw new RuntimeException(e);
				}
			}
		}
	}

	private void commit() {
		var transObject = TransactionObjectHolder.get();
		if (transObject != null) {
			var connMap = transObject.getConnnMap();
			for (Connection conn : connMap.values()) {
				try {
					conn.commit();
				} catch (SQLException e) {
					log.info("回滚失败");
					throw new RuntimeException(e);
				}
			}
		}
	}
}
