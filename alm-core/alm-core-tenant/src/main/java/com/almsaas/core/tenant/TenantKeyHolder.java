package com.almsaas.core.tenant;

public class TenantKeyHolder {

	private static final ThreadLocal<String> tenantKeyHolder = new ThreadLocal<>();

	public static String getTenantKey() {
		return tenantKeyHolder.get();
	}

	public static void setTenantKey(String tenantKey) {
		tenantKeyHolder.set(tenantKey);
	}

	public static void setEmpty() {
		tenantKeyHolder.set("");
	}
}
