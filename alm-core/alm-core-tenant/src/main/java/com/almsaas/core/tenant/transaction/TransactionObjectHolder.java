package com.almsaas.core.tenant.transaction;

public class TransactionObjectHolder {

	private static ThreadLocal<TransactionObject> threadLocal = new ThreadLocal<>();

	public static TransactionObject get() {
		return threadLocal.get();
	}

	public static void set(TransactionObject transactionObject) {
		threadLocal.set(transactionObject);
	}

	public static void setEmpty() {
		threadLocal.set(null);
	}
}
