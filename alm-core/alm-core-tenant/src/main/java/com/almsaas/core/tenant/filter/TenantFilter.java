package com.almsaas.core.tenant.filter;

import com.almsaas.core.tenant.TenantKeyHolder;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.core.annotation.Order;

import java.io.IOException;

@Order(1)
public class TenantFilter implements Filter {
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		String tenantKey = request.getHeader("TENANT_KEY");
		TenantKeyHolder.setTenantKey(tenantKey);
		filterChain.doFilter(servletRequest, servletResponse);
		TenantKeyHolder.setEmpty();
	}
}
