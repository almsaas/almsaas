package com.almsaas.core.tenant.transaction;

import lombok.Data;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

@Data
public class TransactionObject {

	private boolean beginTransaction;

	private Map<DataSource, Connection> connnMap = new HashMap<>();

	public void addConn(DataSource dataSource, Connection conn) {
		connnMap.put(dataSource, conn);
	}
}
