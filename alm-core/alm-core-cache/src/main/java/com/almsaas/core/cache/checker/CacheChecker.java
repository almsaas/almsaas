package com.almsaas.core.cache.checker;

public interface CacheChecker {


	void register(String cacheKey, String cacheEntity);


	boolean check(String cacheKey, String cacheEntity);
}
