package com.almsaas.core.cache;

public interface Cache {

	/**
	 * 缓存
	 * @param cacheKey
	 * @param entity
	 * @return
	 */
	Object get(String cacheKey, String entity);
}
