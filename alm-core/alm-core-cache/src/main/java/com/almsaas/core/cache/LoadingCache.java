package com.almsaas.core.cache;

import com.almsaas.core.cache.checker.CacheChecker;
import com.almsaas.core.cache.checker.CacheCheckerLoader;

import java.util.*;

public class LoadingCache implements Cache {

	private CacheChecker cacheChecker;
	private CacheProvider cacheProvider;
	private int maxSize;
	private Map<String, CacheEntry> cache = new HashMap<>();

	public LoadingCache(CacheProvider cacheProvider) {
		this.cacheProvider = cacheProvider;
		ServiceLoader<CacheCheckerLoader> serviceLoader = ServiceLoader.load(CacheCheckerLoader.class);
		List<CacheCheckerLoader> cacheCheckerLoaders = new ArrayList<>();
		if (serviceLoader == null) {
			throw new RuntimeException("can not find CacheCheckerLoader");
		}
		serviceLoader.forEach(item -> cacheCheckerLoaders.add(item));
		if (cacheCheckerLoaders.size() == 0) {
			throw new RuntimeException("can not find CacheCheckerLoader");
		}
		this.cacheChecker = cacheCheckerLoaders.get(0).loadChecker();
	}

	@Override
	public Object get(String cacheKey, String entity) {
		// 需要去check缓存是否被更新了
		cacheChecker.register(cacheKey, entity);
		boolean isNeedRefresh = cacheChecker.check(cacheKey, entity);

		return null;
	}
}
