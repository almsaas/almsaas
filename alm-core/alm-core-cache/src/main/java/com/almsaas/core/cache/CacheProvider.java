package com.almsaas.core.cache;

public interface CacheProvider {

	/**
	 * laod cache
	 * @param cacheKey
	 * @return
	 */
	Object load(String cacheKey);
}
