package com.almsaas.core.cache;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public class CacheItem {

	@Getter
	private Object value;

	@Getter
	private int version;

	public CacheItem(Object value, int version) {
		this.value = value;
		this.version = version;
	}
}
