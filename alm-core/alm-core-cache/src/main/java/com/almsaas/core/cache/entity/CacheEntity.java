package com.almsaas.core.cache.entity;

import lombok.Data;

import java.util.Date;

@Data
public class CacheEntity {

	private Long id;

	private String cacheEntity;

	private String cacheKey;

	private Integer version;

	private Date createTime;

	private Date updateTime;
}
