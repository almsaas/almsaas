package com.almsaas.core.cache.mapper;

import org.apache.ibatis.annotations.Update;

public interface CacheMapper {

	@Update("update alms_cache set version = version + 1 where cache_key = #{cacheKey} and cache_entity = #{cacheEntity}")
	int refresh(String cacheKey, String cacheEntity);


	int getVersion(String cacheKey, String cacheEntity);
}
