package com.almsaas.core.common.api;

import lombok.Data;

@Data
public class Result<T>{

	private int code;

	private String msg;

	private T data;

	public Result(int code, String msg) {
		this(code, msg, null);
	}

	public Result(int code, String msg, T data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public static Result success(Object data) {
		return new Result(200, "OK", data);
	}

	public static Result success() {
		return new Result(200, "OK");
	}

	public static Result fail(String msg) {
		return new Result(400, msg);
	}

	public static Result fail(int code, String msg) {
		return new Result(code, msg);
	}
}
