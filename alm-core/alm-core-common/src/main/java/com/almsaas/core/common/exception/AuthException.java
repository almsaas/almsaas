package com.almsaas.core.common.exception;

public class AuthException extends RuntimeException {

	public AuthException(String msg) {
		super(msg);
	}
}
