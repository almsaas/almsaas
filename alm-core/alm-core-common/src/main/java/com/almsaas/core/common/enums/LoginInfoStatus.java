package com.almsaas.core.common.enums;

import lombok.Getter;

public enum LoginInfoStatus {

	LOGIN(1, "登陆"),
	LOGINOUT(2, "登出");

	@Getter
	private int code;
	private String msg;

	LoginInfoStatus(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}
}
