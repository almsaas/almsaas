package com.almsaas.core.common.enums;

import lombok.Getter;

public enum SmsType {

	SMS_CODE(1, "验证码"),
	SMS_MSG(2, "消息");

	@Getter
	private int code;
	private String msg;

	SmsType(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}


}
