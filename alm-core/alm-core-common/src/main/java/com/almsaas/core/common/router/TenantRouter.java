package com.almsaas.core.common.router;

import java.lang.annotation.*;

/**
 * 租户路由
 */
@Target(value = {ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface TenantRouter {
}
