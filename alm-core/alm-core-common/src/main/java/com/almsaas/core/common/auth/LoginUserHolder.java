package com.almsaas.core.common.auth;

public class LoginUserHolder {

	private static ThreadLocal<LoginUser> threadLocal = new ThreadLocal<>();

	public static <T> LoginUser<T> get() {
		return threadLocal.get();
	}

	public static void set(LoginUser loginUser) {
		threadLocal.set(loginUser);
	}
}
