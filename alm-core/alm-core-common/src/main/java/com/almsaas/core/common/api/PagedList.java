package com.almsaas.core.common.api;

import lombok.Data;

import java.util.List;

@Data
public class PagedList<T> {

	private List<T> list;

	private int currentPage;

	private int pageSize;

	private int totalCount;

	private int totalPages;
}
