package com.almsaas.core.common.entity;

import cn.hutool.core.util.StrUtil;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.core.keygen.KeyGenerators;
import lombok.Data;

import java.util.Date;

@Data
public class BaseEntity {

	public static final String SYSTEM = "system";

	@Id(keyType = KeyType.Generator, value = KeyGenerators.flexId)
	private Long id;

	@Column("create_user")
	private String createUser;

	@Column("create_time")
	private Date createTime;

	@Column("update_user")
	private String updateUser;

	@Column("update_time")
	private Date updateTime;

	@Column("deleted")
	private Long deleted;

	public void fillInsert() {
		createTime = new Date();
		updateTime = new Date();
		if (StrUtil.isBlank(createUser)) {
			createUser = SYSTEM;
		}
		if (StrUtil.isBlank(updateUser)) {
			updateUser = SYSTEM;
		}
	}
}
