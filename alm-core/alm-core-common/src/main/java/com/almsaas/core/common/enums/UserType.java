package com.almsaas.core.common.enums;

import lombok.Getter;

public enum UserType {

	USER(1, "用户"),
	STAFF(2, "员工");

	@Getter
	private int code;
	private String msg;

	UserType(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}
}
