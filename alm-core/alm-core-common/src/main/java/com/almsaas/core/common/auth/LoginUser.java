package com.almsaas.core.common.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Date;

@Data
public class LoginUser<T> {

	private Long loginId;

	private Integer loginType;

	private String loginIp;

	private Date loginTime;

	private String userJson;

	@JsonIgnore
	private T user;
}
