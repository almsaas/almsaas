package com.almsaas.core.common.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseHandler<T extends BizInput, V extends BizOutput> {

	private static final Logger logger = LoggerFactory.getLogger(BaseHandler.class);

	/**
	 * 处理业务
	 * @param params
	 * @return
	 */
	public V handle(T params) {
		return null;
	}
}
