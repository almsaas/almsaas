package com.almsaas.core.mybatis;

import com.mybatisflex.core.dialect.IDialect;
import com.mybatisflex.core.logicdelete.impl.PrimaryKeyLogicDeleteProcessor;
import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.query.QueryTable;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.table.TableInfo;

public class LogicDeleteProcessor extends PrimaryKeyLogicDeleteProcessor {

	@Override
	public String buildLogicNormalCondition(String logicColumn, TableInfo tableInfo, IDialect dialect) {
		return dialect.wrap(logicColumn) + " = 0";
	}

	@Override
	public void buildQueryCondition(QueryWrapper queryWrapper, TableInfo tableInfo, String joinTableAlias) {
		QueryTable queryTable = new QueryTable(tableInfo.getSchema(), tableInfo.getTableName()).as(joinTableAlias);
		QueryColumn queryColumn = new QueryColumn(queryTable, tableInfo.getLogicDeleteColumn());
		queryWrapper.and(queryColumn.eq(0));
	}

	@Override
	public Object getLogicNormalValue() {
		return 0;
	}
}
