package com.als.core.sms;

public interface SmsSender {

	/**
	 * 发送验证码
	 * @param phone
	 * @param smsCode
	 */
	void sendCode(String phone, String smsCode);

	/**
	 * 发送短信消息
	 * @param phone
	 * @param message
	 */
	void sendMessage(String phone, String message);
}
