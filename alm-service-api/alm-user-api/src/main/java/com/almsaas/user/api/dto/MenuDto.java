package com.almsaas.user.api.dto;

import lombok.Data;

import java.util.List;

@Data
public class MenuDto {

	private Long id;
	private Long parentId;
	private String name;
	private String icon;
	private List<MenuDto> childs;
}
