package com.almsaas.platform.api.dto;

import lombok.Data;

@Data
public class TenantDto {

	private Long id;
	private String name;
	private String tenantKey;
	private String jdbcUrl;
	private String userName;
	private String password;
}
