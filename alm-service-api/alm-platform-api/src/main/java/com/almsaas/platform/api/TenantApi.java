package com.almsaas.platform.api;

import com.almsaas.platform.api.dto.TenantDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(value = "alm-platform")
public interface TenantApi {

	/**
	 * 拿到所有的租户信息
	 * @return
	 */
	@GetMapping("/rest/tenant/list")
	List<TenantDto> list();
}
