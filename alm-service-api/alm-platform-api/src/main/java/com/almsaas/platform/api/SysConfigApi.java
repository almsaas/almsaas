package com.almsaas.platform.api;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "alm-platform")
public interface SysConfigApi {
}
