package com.almsaas.platform.api.dto;

import lombok.Data;

@Data
public class SysConfig {

	private Integer bizType;

	private String key;

	private String value;
}
