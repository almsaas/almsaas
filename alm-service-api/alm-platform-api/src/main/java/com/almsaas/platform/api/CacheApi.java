package com.almsaas.platform.api;

import com.almsaas.platform.api.dto.CacheRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "alm-platform")
public interface CacheApi {

	/**
	 * 查询缓存是否需要刷新
	 * @param cacheKey
	 * @param entity
	 * @return
	 */
	@PostMapping("/rest/cache/check")
	boolean check(@RequestBody CacheRequest cacheRequest);

	/**
	 * 刷新缓存
	 * @param cacheKey
	 * @param entity
	 */
	@PostMapping("/rest/cache/refresh")
	void refresh(@RequestBody CacheRequest cacheRequest);
}
