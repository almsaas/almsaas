package com.almsaas.gateway.web.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients("com.almsaas.platform.api")
public class FeignConfiguration {
}
