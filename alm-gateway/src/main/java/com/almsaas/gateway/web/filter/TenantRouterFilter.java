package com.almsaas.gateway.web.filter;

import com.almsaas.platform.api.TenantApi;
import com.almsaas.platform.api.dto.TenantDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Slf4j
public class TenantRouterFilter implements GlobalFilter {

	@Autowired
	@Lazy
	private TenantApi tenantApi;

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		InetSocketAddress host = exchange.getRequest().getLocalAddress();
		String originalDomain = host.getHostName();
		log.info("当前请求域名: {}", originalDomain);
//		List<TenantDto> tenants = tenantApi.list();
//		Map<String, TenantDto> tenantDtoMap = tenants.stream()
//			.collect(Collectors.toMap(TenantDto::getTenantKey, item -> item));
//		exchange.getRequest().getHeaders().set("TENANT_KEY", originalDomain);
		return chain.filter(exchange);
	}
}
